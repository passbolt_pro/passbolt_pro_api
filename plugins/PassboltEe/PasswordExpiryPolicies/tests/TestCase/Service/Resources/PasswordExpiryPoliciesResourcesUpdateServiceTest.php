<?php
declare(strict_types=1);

/**
 * Passbolt ~ Open source password manager for teams
 * Copyright (c) Passbolt SA (https://www.passbolt.com)
 *
 * Licensed under GNU Affero General Public License version 3 of the or any later version.
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Passbolt SA (https://www.passbolt.com)
 * @license       https://opensource.org/licenses/AGPL-3.0 AGPL License
 * @link          https://www.passbolt.com Passbolt(tm)
 * @since         4.5.0
 */

namespace Passbolt\PasswordExpiryPolicies\Test\TestCase\Service\Resources;

use App\Error\Exception\ValidationException;
use App\Service\Resources\PasswordExpiryValidationServiceInterface;
use App\Service\Resources\ResourcesUpdateService;
use App\Test\Factory\ResourceFactory;
use App\Test\Factory\UserFactory;
use App\Test\Lib\AppTestCase;
use Cake\I18n\FrozenTime;
use Passbolt\Metadata\Model\Dto\MetadataResourceDto;
use Passbolt\PasswordExpiryPolicies\Test\Factory\PasswordExpiryPoliciesSettingFactory;
use Passbolt\ResourceTypes\Test\Factory\ResourceTypeFactory;

class PasswordExpiryPoliciesResourcesUpdateServiceTest extends AppTestCase
{
    public ResourcesUpdateService $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = new ResourcesUpdateService();
        ResourceTypeFactory::make()->default()->persist();
    }

    public function tearDown(): void
    {
        unset($this->service);
        parent::tearDown();
    }

    public function testPasswordExpiryPoliciesResourcesUpdateService_Update_With_Expiry_Date_Set_To_Null()
    {
        // Enable the pwd expiry in settings
        PasswordExpiryPoliciesSettingFactory::make()->persist();

        // Arrange
        $owner = UserFactory::make()->user()->persist();
        // Create an expired resource
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::make()
            ->expired()
            ->withPermissionsFor([$owner])
            ->persist();
        $this->assertTrue($resource->isExpired());

        $newName = 'Nouveau nom de resource privée';
        $payload = [
            'name' => $newName,
            PasswordExpiryValidationServiceInterface::PASSWORD_EXPIRED_DATE => null,
        ];
        $dto = MetadataResourceDto::fromArray($payload);
        $this->service->update($this->makeUac($owner), $resource->id, $dto);

        // Assert
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::find()->firstOrFail();
        $this->assertFalse($resource->isExpired());
        $this->assertSame($newName, $resource->name);
    }

    public function testPasswordExpiryPoliciesResourcesUpdateService_Update_With_Expiry_Date_Set_To_Future()
    {
        // Enable the pwd expiry in settings
        PasswordExpiryPoliciesSettingFactory::make()->persist();

        // Arrange
        $owner = UserFactory::make()->user()->persist();
        // Create an expired resource
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::make()
            ->expired()
            ->withPermissionsFor([$owner])
            ->persist();
        $this->assertTrue($resource->isExpired());

        $newName = 'Nouveau nom de resource privée';
        $payload = [
            'name' => $newName,
            PasswordExpiryValidationServiceInterface::PASSWORD_EXPIRED_DATE => FrozenTime::tomorrow()->toAtomString(),
        ];
        $dto = MetadataResourceDto::fromArray($payload);
        $this->service->update($this->makeUac($owner), $resource->id, $dto);

        // Assert
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::find()->firstOrFail();
        $this->assertFalse($resource->isExpired());
        $this->assertSame($newName, $resource->name);
    }

    public function testPasswordExpiryPoliciesResourcesUpdateService_Update_With_Expiry_Date_ISO()
    {
        // Enable the pwd expiry in settings
        PasswordExpiryPoliciesSettingFactory::make()->persist();

        // Arrange
        $owner = UserFactory::make()->user()->persist();
        // Create an expired resource
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::make()
            ->withPermissionsFor([$owner])
            ->persist();

        // Time in the format used by the Bext
        $expiryDate = '2000-01-01T12:30:52.689Z';

        $newName = 'Nouveau nom de resource privée';
        $payload = [
            'name' => $newName,
            PasswordExpiryValidationServiceInterface::PASSWORD_EXPIRED_DATE => $expiryDate,
        ];
        $dto = MetadataResourceDto::fromArray($payload);
        $this->service->update($this->makeUac($owner), $resource->id, $dto);

        // Assert
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::find()->firstOrFail();
        $this->assertTrue($resource->isExpired());
        $this->assertSame($newName, $resource->name);
    }

    public function testPasswordExpiryPoliciesResourcesUpdateService_Update_With_Expiry_Not_Null()
    {
        // Enable the pwd expiry in settings
        PasswordExpiryPoliciesSettingFactory::make()->persist();

        // Arrange
        $owner = UserFactory::make()->user()->persist();
        // Create an expired resource
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::make()
            ->expired()
            ->withPermissionsFor([$owner])
            ->persist();
        $this->assertTrue($resource->isExpired());

        $payload = [
            PasswordExpiryValidationServiceInterface::PASSWORD_EXPIRED_DATE => 'Foo',
        ];
        $dto = MetadataResourceDto::fromArray($payload);

        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Could not validate resource data.');
        $this->service->update($this->makeUac($owner), $resource->id, $dto);
    }

    public function testPasswordExpiryPoliciesResourcesUpdateService_Update_With_Settings_Not_Set()
    {
        // Arrange
        $owner = UserFactory::make()->user()->persist();
        // Create an expired resource
        /** @var \App\Model\Entity\Resource $resource */
        $resource = ResourceFactory::make()
            ->expired()
            ->withPermissionsFor([$owner])
            ->persist();
        $this->assertTrue($resource->isExpired());

        $payload = [
            PasswordExpiryValidationServiceInterface::PASSWORD_EXPIRED_DATE => null,
        ];
        $dto = MetadataResourceDto::fromArray($payload);
        // Although the password expiry is deactivated, the password is now marked as not expired
        $resource = $this->service->update($this->makeUac($owner), $resource->id, $dto);
        $this->assertFalse($resource->isExpired());
    }
}
