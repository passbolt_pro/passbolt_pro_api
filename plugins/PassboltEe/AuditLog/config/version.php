<?php
return [
    'passbolt' => [
        'plugins' => [
            'auditLog' => [
                'version' => '1.0.0',
            ],
            // @deprecated remove v4
            'audit_log' => [
                'version' => '1.0.0',
            ],
        ],
    ],
];
