<?php

/**
 * Passbolt ~ Open source password manager for teams
 * Copyright (c) Passbolt SARL (https://www.passbolt.com)
 *
 * Licensed under GNU Affero General Public License version 3 of the or any later version.
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Passbolt SARL (https://www.passbolt.com)
 * @license       https://opensource.org/licenses/AGPL-3.0 AGPL License
 * @link          https://www.passbolt.com Passbolt(tm)
 * @since         4.10.0
 */

use Cake\Core\Configure;

return [
    'passbolt' => [
        'plugins' => [
            'subscription' => [
                'version' => '1.0.0',
                'enabled' => true,
                'subscriptionKey' => [
                    'public' => Configure::read(
                        'passbolt.plugins.subscription.subscriptionKey.public',
                        __DIR__ . DS . 'subscription_public.key',
                    ),
                ],
            ],
        ],
    ],
];
