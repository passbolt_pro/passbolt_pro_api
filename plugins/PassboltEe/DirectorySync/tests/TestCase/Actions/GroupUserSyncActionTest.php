<?php
declare(strict_types=1);

/**
 * Passbolt ~ Open source password manager for teams
 * Copyright (c) Passbolt SA (https://www.passbolt.com)
 *
 * Licensed under GNU Affero General Public License version 3 of the or any later version.
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Passbolt SA (https://www.passbolt.com)
 * @license       https://opensource.org/licenses/AGPL-3.0 AGPL License
 * @link          https://www.passbolt.com Passbolt(tm)
 * @since         2.0.0
 */
namespace Passbolt\DirectorySync\Test\TestCase\Actions;

use App\Command\CommandBootstrap;
use App\Notification\Email\EmailSubscriptionDispatcher;
use App\Notification\Email\Redactor\CoreEmailRedactorPool;
use App\Notification\Email\Redactor\Group\GroupUserAddRequestEmailRedactor;
use App\Service\Resources\ResourcesExpireResourcesFallbackServiceService;
use App\Test\Lib\Model\EmailQueueTrait;
use App\Utility\UuidFactory;
use Cake\Core\Configure;
use Cake\Event\EventManager;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Passbolt\DirectorySync\Actions\GroupSyncAction;
use Passbolt\DirectorySync\Test\Utility\DirectorySyncDeprecatedIntegrationTestCase;
use Passbolt\DirectorySync\Test\Utility\Traits\AssertDirectoryRelationsTrait;
use Passbolt\DirectorySync\Test\Utility\Traits\AssertGroupsTrait;
use Passbolt\DirectorySync\Test\Utility\Traits\AssertGroupUsersTrait;
use Passbolt\DirectorySync\Utility\Alias;
use Passbolt\EmailNotificationSettings\Test\Lib\EmailNotificationSettingsTestTrait;

/**
 * @covers \Passbolt\DirectorySync\Actions\GroupSyncAction
 */
class GroupUserSyncActionTest extends DirectorySyncDeprecatedIntegrationTestCase
{
    use AssertDirectoryRelationsTrait;
    use AssertGroupsTrait;
    use AssertGroupUsersTrait;
    use EmailNotificationSettingsTestTrait;
    use EmailQueueTrait;

    public function setUp(): void
    {
        parent::setUp();

        $this->initAction();
        // Enable email notifications
        $this->loadNotificationSettings();
        $this->setEmailNotificationSetting('send.group.manager.requestAddUser', true);
        EventManager::instance()->on(new CoreEmailRedactorPool());
        (new EmailSubscriptionDispatcher())->collectSubscribedEmailRedactors();
        // Init CommandBootstrap to handle email notifications.
        CommandBootstrap::init();
    }

    public function tearDown(): void
    {
        $this->unloadNotificationSettings();
        parent::tearDown();
    }

    /**
     * Init the action
     *
     * @throws \Exception
     * @return void
     */
    public function initAction()
    {
        $this->action = new GroupSyncAction(
            new ResourcesExpireResourcesFallbackServiceService()
        );
        $this->action->getDirectory()->setGroups([]);
    }

    /**
     * Test that orphans group user are cleaned up
     */
    public function testCleanupOrphanDirectoryRelation()
    {
        $this->mockDirectoryEntryGroup('network', null, null, null, null, UuidFactory::uuid('group.id.network'));
        $this->mockDirectoryEntryGroup('accounting', null, null, null, null, UuidFactory::uuid('group.id.accounting'));
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'ada', 'lname' => 'lovelace', 'foreign_key' => UuidFactory::uuid('user.id.ada')]);
        $this->mockDirectoryRelationGroupUser('network', 'ada');
        $relationAccounting = $this->mockDirectoryRelationGroupUser('accounting', 'ada');
        $this->mockDirectoryUserData('ada', 'lovelace', 'ada@passbolt.com', new FrozenTime('now'), new FrozenTime('now'));
        $this->mockDirectoryGroupData('network');
        $this->mockDirectoryGroupData('accounting', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        $reports = $this->action->execute();
        $this->assertReportEmpty($reports);
        $this->assertDirectoryEntryCount(3);
        $this->assertDirectoryRelationCount(1);
        $this->assertGroupUserExist($relationAccounting->id);
        $this->assertDirectoryRelationExist($relationAccounting->id);
    }

    /**
     * Scenario: A group was deleted but no group users were ever created
     * Expected result: do nothing
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case2_Null_Null_Null_Ok_Any()
    {
        $this->mockDirectoryEntryGroup('marketing');
        $reports = $this->action->execute();
        $this->assertReportNotEmpty($reports);
        $this->assertNoReportsForModel($reports, Alias::MODEL_GROUPS_USERS);
        $this->assertGroupNotExist(UuidFactory::uuid('group.id.marketing'), ['deleted' => false]);
        $this->assertDirectoryRelationEmpty();
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: A group was deleted and some groupUsers were already synced, but have been removed at passbolt end
     * Expected result: remove directory relation
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case3_Null_Null_Ok_Null_Any()
    {
        $relation = $this->mockDirectoryRelationGroupUser('accounting', 'edith');
        $reports = $this->action->execute();
        $this->assertReportEmpty($reports);
        $this->assertGroupExist(UuidFactory::uuid('group.id.accounting'), ['deleted' => false]);
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.accounting'), 'user_id' => UuidFactory::uuid('user.id.edith')]);
        $this->assertDirectoryRelationNotExist($relation->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: a group was deleted in ldap and its group users were synced
     * Expected result: remove group user and entry, send report
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case4_Null_Null_Ok_Ok_Any()
    {
        $this->mockDirectoryEntryGroup('freelancer', null, null, null, null, UuidFactory::uuid('group.id.freelancer'));
        $relation = $this->mockDirectoryRelationGroupUser('freelancer', 'frances');
        $reports = $this->action->execute();
        $this->assertEquals(count($reports), 1);
        $expectedReport = [
            'action' => Alias::ACTION_DELETE,
            'model' => Alias::MODEL_GROUPS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedReport);
        $this->assertNoReportsForModel($reports, Alias::MODEL_GROUPS_USERS);
        $this->assertGroupNotExist(UuidFactory::uuid('group.id.freelancer'), ['deleted' => false]);
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.freelancer'), 'user_id' => UuidFactory::uuid('user.id.frances')]);
        $this->assertDirectoryRelationNotExist($relation->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: a groupUser was deleted in ldap who is the sole manager of the group
     * Expected result: do nothing, send error report
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case5_Ok_Null_Ok_NotDeletable_Any()
    {
        $this->mockDirectoryEntryGroup('accounting', null, null, null, null, UuidFactory::uuid('group.id.accounting'));
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'ada', 'lname' => 'lovelace', 'foreign_key' => UuidFactory::uuid('user.id.ada')]);
        $relation = $this->mockDirectoryRelationGroupUser('accounting', 'ada');
        $userData = $this->mockDirectoryUserData('ada', 'lovelace', 'ada@passbolt.com', new FrozenTime('now'), new FrozenTime('now'));
        $groupData = $this->mockDirectoryGroupData('accounting');

        $reports = $this->action->execute();
        $this->assertEquals(count($reports), 1);
        $expectedReport = [
            'action' => Alias::ACTION_DELETE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_ERROR,
            'type' => 'SyncError',
        ];
        $this->assertReport($reports[0], $expectedReport);
        $this->assertGroupExist(UuidFactory::uuid('group.id.accounting'), ['deleted' => false]);
        $this->assertGroupUserExist($relation->id);
        $this->assertDirectoryRelationExist($relation->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: a group has been created without group users
     * Expected result: create group and add default group user as group manager
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case6_Ok_Null_Null_Null_Any()
    {
        $this->mockDirectoryGroupData('newgroup');
        $reports = $this->action->execute();
        $this->assertReportNotEmpty($reports);
        $this->assertEquals(count($reports), 1);
        $expectedReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedReport);
        $groupCreated = $this->assertGroupExist(null, ['name' => 'newgroup', 'deleted' => false]);

        $defaultGroupAdmin = $this->directoryOrgSettings->getDefaultGroupAdminUser();
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();

        $groupUser = $this->assertGroupUserExist(null, ['group_id' => $groupCreated->id, 'user_id' => $defaultGroupAdmin->get('id')]);
        $this->assertDirectoryRelationNotExist($groupUser->id);
        $this->assertEmailIsInQueue([
            'email' => 'ada@passbolt.com',
            'subject' => 'Admin added you to the group newgroup',
            'template' => 'LU/group_user_add',
        ]);
        $this->assertEmailQueueCount(1);
    }

    /**
     * Scenario: a group has been created without group users in ldap, and already contains a matching group and group users in passbolt
     * Expected result: do nothing
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case7_Ok_Null_Null_Ok_Any()
    {
        $groupData = $this->mockDirectoryGroupData('newgroup');
        $this->mockDirectoryEntryUser(['fname' => 'ada', 'lname' => 'ada', 'foreign_key' => UuidFactory::uuid('user.id.ada')]);

        $reports = $this->action->execute();

        $this->assertEquals(count($reports), 1);
        $expectedReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS,
            'status' => Alias::STATUS_SUCCESS,
        ];
        $this->assertReport($reports[0], $expectedReport);
        $groupCreated = $this->assertGroupExist(null, ['name' => 'newgroup', 'deleted' => false]);

        $defaultGroupAdmin = $this->directoryOrgSettings->getDefaultGroupAdminUser();
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();

        $groupUser = $this->assertGroupUserExist(null, ['group_id' => $groupCreated->id, 'user_id' => $defaultGroupAdmin->get('id')]);
        $this->assertDirectoryRelationNotExist($groupUser->id);
        $this->assertEmailIsInQueue([
            'email' => 'ada@passbolt.com',
            'subject' => 'Admin added you to the group newgroup',
            'template' => 'LU/group_user_add',
        ]);
        $this->assertEmailQueueCount(1);
    }

    /**
     * Scenario: a group has group users removed in ldap but these group users were removed in passbolt
     * Expected result: remove directory relation
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case8_Ok_Null_Ok_Null_Any()
    {
        $this->mockDirectoryEntryGroup('freelancer', null, null, null, null, UuidFactory::uuid('group.id.freelancer'));
        $relation = $this->mockDirectoryRelationGroupUser('freelancer', 'ada');
        $this->mockDirectoryGroupData('freelancer');
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.freelancer'), 'user_id' => UuidFactory::uuid('user.id.ada')]);
        $reports = $this->action->execute();
        $this->assertTrue($reports->isEmpty());
        $this->assertGroupExist(UuidFactory::uuid('group.id.freelancer'), ['deleted' => false]);
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.freelancer'), 'user_id' => UuidFactory::uuid('user.id.ada')]);
        $this->assertDirectoryRelationNotExist($relation->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: a group user that was already synced has been removed in ldap
     * Expected result: remove group user in passbolt and an email is sent to user notifying they have been removed from the group
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case9_Ok_Null_Ok_Ok_Ok()
    {
        $this->mockDirectoryEntryGroup('accounting', null, null, null, null, UuidFactory::uuid('group.id.accounting'));
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'betty', 'lname' => 'betty', 'foreign_key' => UuidFactory::uuid('user.id.betty')]);
        $relation = $this->mockDirectoryRelationGroupUser('accounting', 'betty');
        $userData = $this->mockDirectoryUserData('betty', 'betty', 'betty@passbolt.com', new FrozenTime('now'), new FrozenTime('now'));
        $groupData = $this->mockDirectoryGroupData('accounting');

        $reports = $this->action->execute();

        $this->assertEquals(count($reports), 1);
        $expectedReport = [
            'action' => Alias::ACTION_DELETE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedReport);
        $this->assertGroupExist(UuidFactory::uuid('group.id.accounting'), ['deleted' => false]);

        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.accounting'), 'user_id' => UuidFactory::uuid('user.id.betty')]);
        $this->assertDirectoryRelationNotExist($relation->id);
        $this->assertEmailQueueCount(2);
        $this->assertEmailInBatchContains('removed you from the group');
    }

    /**
     * Scenario: a group has a groupUser in ldap which has not been synced yet, but the corresponding user doesn't exist, has been deleted or is unactive.
     * Expected result: do nothing, send ignore report
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case10_Ok_Ok_Null_Null_NotOk()
    {
        // Ruth is a inactive user.
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'ruth', 'lname' => 'ruth', 'foreign_key' => UuidFactory::uuid('user.id.ruth')]);
        $this->mockDirectoryUserData('ruth', 'ruth', 'ruth@passbolt.com');
        $this->mockDirectoryGroupData('newgroup', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);
        $reports = $this->action->execute();
        $this->assertReportNotEmpty($reports);
        $this->assertEquals(count($reports), 2);
        $expectedGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedGroupReport);

        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_IGNORE,
            'type' => Alias::MODEL_GROUPS,
            'message' => 'The user ruth@passbolt.com could not be added to the group newgroup because they have not yet activated their account.',
        ];
        $this->assertReport($reports[1], $expectedUserGroupReport);

        $groupCreated = $this->assertGroupExist(null, ['name' => 'newgroup', 'deleted' => false]);

        $defaultGroupAdmin = $this->directoryOrgSettings->getDefaultGroupAdminUser();
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();

        $groupUserAda = $this->assertGroupUserExist(null, ['group_id' => $groupCreated->id, 'user_id' => $defaultGroupAdmin->get('id')]);
        $this->assertGroupUserNotExist(null, ['group_id' => $groupCreated->id, 'user_id' => UuidFactory::uuid('user.id.ruth')]);
        $this->assertDirectoryRelationEmpty();
        $this->assertEmailIsInQueue([
            'email' => 'ada@passbolt.com',
            'subject' => 'Admin added you to the group newgroup',
            'template' => 'LU/group_user_add',
        ]);
        $this->assertEmailQueueCount(1);
    }

    /**
     * Scenario: a group has two groupUsers in ldap which has not been synced yet, one of the corresponding user doesn't exist, has been deleted or is unactive.
     * Expected result: add first user, send ignore report for second one
     *
     * This is
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case10_Ok_Ok_Null_Null_NotOk_WithTwoRows()
    {
        $userEntryValid = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        // Ruth is a inactive user.
        $userEntryInactive = $this->mockDirectoryEntryUser(['fname' => 'ruth', 'lname' => 'ruth', 'foreign_key' => UuidFactory::uuid('user.id.ruth')]);
        $this->mockDirectoryUserData('ruth', 'ruth', 'ruth@passbolt.com');
        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $this->mockDirectoryGroupData('newgroup', [
            'group_users' => [
                $userEntryValid->directory_name,
                $userEntryInactive->directory_name,
            ],
        ]);
        $reports = $this->action->execute();

        $this->assertReportNotEmpty($reports);
        $this->assertEquals(count($reports), 3);

        $expectedGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
            'message' => 'The group newgroup was successfully added to passbolt.',
        ];
        $this->assertReport($reports[0], $expectedGroupReport);

        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
            'message' => 'The user frances@passbolt.com was successfully added to the group newgroup.',
        ];
        $this->assertReport($reports[1], $expectedUserGroupReport);

        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_IGNORE,
            'type' => Alias::MODEL_GROUPS,
            'message' => 'The user ruth@passbolt.com could not be added to the group newgroup because they have not yet activated their account.',
        ];
        $this->assertReport($reports[2], $expectedUserGroupReport);

        $groupCreated = $this->assertGroupExist(null, ['name' => 'newgroup', 'deleted' => false]);

        $defaultGroupAdmin = $this->directoryOrgSettings->getDefaultGroupAdminUser();
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();

        $this->assertGroupUserExist(null, ['group_id' => $groupCreated->id, 'user_id' => $defaultGroupAdmin->get('id')]);
        $this->assertGroupUserNotExist(null, ['group_id' => $groupCreated->id, 'user_id' => UuidFactory::uuid('user.id.ruth')]);
        $this->assertDirectoryRelationNotEmpty();
        $this->assertEmailIsInQueue([
            'email' => 'ada@passbolt.com',
            'subject' => 'Admin added you to the group newgroup',
            'template' => 'LU/group_user_add',
        ]);
        $this->assertEmailQueueCount(1);
    }

    /**
     * Scenario: a groupUser has been added to a group without passwords in ldap, not yet added in passbolt
     * Expected result: check if group already has access to passwords. If not, add the groupUser. If yes, send notification to groupAdmins to add user.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case11_Ok_Ok_Null_Null_Ok()
    {
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $groupData = $this->mockDirectoryGroupData('newgroup', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        $reports = $this->action->execute();
        $this->assertNotEmpty($reports);
        $this->assertEquals(count($reports), 2);
        $expectedGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedGroupReport);

        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[1], $expectedUserGroupReport);
        $groupCreated = $this->assertGroupExist(null, ['name' => 'newgroup', 'deleted' => false]);

        $defaultGroupAdmin = $this->directoryOrgSettings->getDefaultGroupAdminUser();
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();

        $groupUserAda = $this->assertGroupUserExist(null, ['group_id' => $groupCreated->id, 'user_id' => $defaultGroupAdmin->get('id')]);
        $groupUserFrances = $this->assertGroupUserExist(null, ['group_id' => $groupCreated->id, 'user_id' => UuidFactory::uuid('user.id.frances')]);
        $this->assertDirectoryRelationNotExist($groupUserAda->id); // Directory relation should not be added for default user.
        $this->assertDirectoryRelationExist($groupUserFrances->id);
        $this->assertEmailIsInQueue([
            'email' => 'ada@passbolt.com',
            'subject' => 'Admin added you to the group newgroup',
            'template' => 'LU/group_user_add',
        ]);
        $this->assertEmailQueueCount(1);
    }

    /**
     * Scenario: a groupUser has been added to a group without passwords in ldap, not yet added in passbolt
     * Expected result: check if group already has access to passwords. If not, add the groupUser. If yes, send notification to groupAdmins to add user.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case11a_Ok_Ok_Null_Null_Ok_Edited_Group_No_Passwords()
    {
        $defaultGroupAdmin = 'edith@passbolt.com';
        $this->setDefaultGroupAdminUser($defaultGroupAdmin);
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();
        $this->initAction();

        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $groupEntry = $this->mockDirectoryEntryGroup('marketing');
        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $groupData = $this->mockDirectoryGroupData('marketing', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        $reports = $this->action->execute();

        $this->assertEquals(count($reports), 1);
        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);

        // Group user for default admin should not exist.
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.marketing'), 'user_id' => $defaultGroupAdmin->get('id')]);

        // Frances should be in group users and directoryRelations.
        $groupUserFrances = $this->assertGroupUserExist(null, ['group_id' => UuidFactory::uuid('group.id.marketing'), 'user_id' => UuidFactory::uuid('user.id.frances')]);
        $this->assertDirectoryRelationExist($groupUserFrances->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: a groupUser has been added to a group without passwords in ldap, not yet added in passbolt
     * Expected result: check if group already has access to passwords. If not, add the groupUser. If yes, send notification to groupAdmins to add user.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case11a_Ok_Ok_Null_Null_Ok_Edited_Group_No_Passwords_UpdateDisabled()
    {
        $this->disableSyncOperation('groups', 'update');
        $this->action = new GroupSyncAction(
            new ResourcesExpireResourcesFallbackServiceService()
        );
        $this->action->getDirectory()->setGroups([]);

        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $this->mockDirectoryEntryGroup('marketing');
        $this->mockDirectoryGroupData('marketing', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        $reports = $this->action->execute();
        $this->assertReportEmpty($reports);
        // Frances should be in group users and directoryRelations.
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.marketing'), 'user_id' => UuidFactory::uuid('user.id.frances')]);
        $this->assertEmailQueueCount(0);
    }

    public function requestMembersAddToManagerProvider(): array
    {
        return [
            [
                'input' => false,
                'expected' => [
                    'subject' => 'Admin requested you to add members to Accounting',
                    'headerText' => 'Admin requested you to add members to Accounting',
                ],
            ],
            [
                'input' => true,
                'expected' => [
                    'subject' => 'You have been requested to add members to Accounting',
                    'headerText' => 'There was a change in the user directory',
                ],
            ],
        ];
    }

    /**
     * Scenario: a groupUser has been added to a group with passwords in ldap, not yet added in passbolt
     * Expected result: Send email notification to groupAdmins to add user.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     * @dataProvider requestMembersAddToManagerProvider
     */
    public function testDirectorySyncGroupUser_Case11b_Ok_Ok_Null_Null_Ok_Edited_Group_With_Passwords(bool $flag, array $expected)
    {
        // Since an email is sent in this test, it is necessary to load the EmailDigest plugin
        // in order to set the email's encryption to JSON
        $this->loadPlugins(['Passbolt/EmailDigest' => []]);
        Configure::write(GroupUserAddRequestEmailRedactor::CONFIG_KEY_ANONYMISE_ADMINISTRATOR_IDENTITY, $flag);

        $defaultGroupAdmin = 'edith@passbolt.com';
        $this->setDefaultGroupAdminUser($defaultGroupAdmin);
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();
        $this->initAction();

        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $this->mockDirectoryEntryGroup('accounting');
        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $this->mockDirectoryGroupData('accounting', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        $reports = $this->action->execute();

        $this->assertEquals(count($reports), 1);
        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_WARNING,
            'type' => Alias::MODEL_USERS,
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);

        // Group user for default admin should not exist.
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.accounting'), 'user_id' => $defaultGroupAdmin->get('id')]);

        // Frances should be in group users and directoryRelations.
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.marketing'), 'user_id' => UuidFactory::uuid('user.id.frances')]);
        $this->assertDirectoryRelationEmpty();

        // Assert email notification
        $this->assertEmailIsInQueue([
            'email' => 'ada@passbolt.com',
            'subject' => $expected['subject'],
            'template' => 'GM/group_user_request',
        ]);
        $this->assertEmailQueueCount(1);
        $this->assertEmailInBatchContains($expected['headerText']);
        $this->assertEmailInBatchContains('Frances Allen (Member)');
    }

    /**
     * PB-1431: Fix: LDAP: a notification should not be sent to a group administrator requesting him to add a non-active user.
     *
     * Scenario: a non active groupUser has been added to a ldap group, not yet added in Passbolt.
     * But the passbolt group has access to shared passwords.
     * Expected result: No email notification should be sent. An ignore report should be broadcasted.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_PB1431()
    {
        // Init CommandBootstrap to handle email notifications.
        CommandBootstrap::init();

        $defaultGroupAdmin = 'edith@passbolt.com';
        $this->setDefaultGroupAdminUser($defaultGroupAdmin);
        $defaultGroupAdmin = $this->Users->findByUsername($defaultGroupAdmin)->first();
        $this->initAction();

        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'ruth', 'lname' => 'ruth', 'foreign_key' => UuidFactory::uuid('user.id.ruth')]);
        $this->mockDirectoryEntryGroup('accounting');
        $this->mockDirectoryUserData('ruth', 'ruth', 'ruth@passbolt.com');
        $this->mockDirectoryGroupData('accounting', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        $reports = $this->action->execute();

        $this->assertEquals(count($reports), 1);
        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_IGNORE,
            'type' => Alias::MODEL_GROUPS,
            'message' => 'The user ruth@passbolt.com could not be added to the group Accounting because they have not yet activated their account.',
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);

        // Group user for default admin should not exist.
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.accounting'), 'user_id' => $defaultGroupAdmin->get('id')]);

        // Frances should not be in group users and directoryRelations.
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.marketing'), 'user_id' => UuidFactory::uuid('user.id.ruth')]);
        $this->assertDirectoryRelationEmpty();

        // No email notification should have been sent to the group manager.
        $this->assertEmailQueueIsEmpty();
    }

    /**
     * Scenario: A groupUser has been added to a group in ldap and already exist in passbolt
     * Expected result: do nothing. ignore report.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case12_Ok_Ok_Null_CreatedBefore_Ok()
    {
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'grace', 'lname' => 'grace', 'foreign_key' => UuidFactory::uuid('user.id.grace')]);
        $groupEntry = $this->mockDirectoryEntryGroup('freelancer');
        $this->mockDirectoryUserData('grace', 'grace', 'grace@passbolt.com');
        $groupData = $this->mockDirectoryGroupData('freelancer', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        // Define creation date.
        $dateGroupModification = $groupData['directory_modified'];
        $dateBeforeGroupModification = $dateGroupModification->subDays(1);

        // Update corresponding GroupUser so it is created before.
        $groupsUsers = TableRegistry::getTableLocator()->get('GroupsUsers');
        $groupsUsers->getConnection()->execute("UPDATE {$groupsUsers->getTable()} SET created = ? WHERE group_id = ? AND user_id = ?", [
            $dateBeforeGroupModification->format('Y-m-d H:i:s'),
            UuidFactory::uuid('group.id.freelancer'),
            UuidFactory::uuid('user.id.grace'),
        ]);
        $reports = $this->action->execute();
        $this->assertReportNotEmpty($reports);
        $this->assertEquals(count($reports), 1);
        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_IGNORE,
            'type' => 'DirectoryEntry',
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);
        // groupUser should exist, but not directory relation since the sync shouldn't have happened.
        $groupUser = $this->assertGroupUserExist(null, ['group_id' => UuidFactory::uuid('group.id.freelancer'), 'user_id' => UuidFactory::uuid('user.id.grace')]);
        $this->assertDirectoryRelationNotExist($groupUser->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: GroupAdmin added the user to a group in passbolt after it was requested from him.
     * Expected result: create corresponding directoryRelation
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case13_Ok_Ok_Null_CreatedAfter_Ok()
    {
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'nancy', 'lname' => 'nancy', 'foreign_key' => UuidFactory::uuid('user.id.nancy')]);
        $groupEntry = $this->mockDirectoryEntryGroup('marketing');
        $this->mockDirectoryUserData('nancy', 'nancy', 'nancy@passbolt.com');
        $groupData = $this->mockDirectoryGroupData('marketing', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);

        // Define creation date.
        $dateGroupModification = $groupData['directory_modified'];
        $dateAfterGroupModification = $dateGroupModification->addDays(1);

        // Update corresponding GroupUser so it is created before.
        $groupsUsers = TableRegistry::getTableLocator()->get('GroupsUsers');
        $groupsUsers->getConnection()->execute("UPDATE {$groupsUsers->getTable()} SET created = ? WHERE group_id = ? AND user_id = ?", [
            $dateAfterGroupModification->format('Y-m-d H:i:s'),
            UuidFactory::uuid('group.id.marketing'),
            UuidFactory::uuid('user.id.nancy'),
        ]);

        $reports = $this->action->execute();
        $this->assertEquals(count($reports), 1);
        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);

        // groupUser should exist, but not directory relation since the sync shouldn't have happened.
        $groupUser = $this->assertGroupUserExist(null, ['group_id' => UuidFactory::uuid('group.id.marketing'), 'user_id' => UuidFactory::uuid('user.id.nancy')]);
        $this->assertDirectoryRelationExist($groupUser->id);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: A groupUser exists in ldap and has already been synced, but has been deleted in passbolt.
     * Expected result: do nothing, send ignore report
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case14_Ok_Ok_Ok_Null_Any()
    {
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'ada', 'lname' => 'ada', 'foreign_key' => UuidFactory::uuid('user.id.ada')]);
        $this->mockDirectoryEntryGroup('freelancer');
        $this->mockDirectoryUserData('ada', 'ada', 'ada@passbolt.com');
        $this->mockDirectoryGroupData('freelancer', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);
        $this->mockDirectoryRelationGroupUser('freelancer', 'ada');

        $reports = $this->action->execute();

        //var_dump($reports);
        $this->assertEquals(count($reports), 1);
        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_IGNORE,
            'type' => 'DirectoryEntry',
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);
        $this->assertGroupUserNotExist(null, ['group_id' => UuidFactory::uuid('group.id.freelancer'), 'user_id' => UuidFactory::uuid('user.id.ada')]);
        $this->assertDirectoryRelationExist(null, [
            'parent_key' => UuidFactory::uuid('ldap.group.id.freelancer'),
            'child_key' => UuidFactory::uuid('ldap.user.id.ada'),
        ]);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Scenario: A group user exists in ldap and has already been synced.
     * Expected result: do nothing. No report.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_Case15_Ok_Ok_Ok_Ok_Any()
    {
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $this->mockDirectoryEntryGroup('freelancer');
        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $this->mockDirectoryGroupData('freelancer', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);
        $this->mockDirectoryRelationGroupUser('freelancer', 'frances');
        $reports = $this->action->execute();
        $this->assertTrue($reports->isEmpty());
        $this->assertGroupUserExist(null, ['group_id' => UuidFactory::uuid('group.id.freelancer'), 'user_id' => UuidFactory::uuid('user.id.frances')]);
        $this->assertDirectoryRelationExist(null, [
            'parent_key' => UuidFactory::uuid('ldap.group.id.freelancer'),
            'child_key' => UuidFactory::uuid('ldap.user.id.frances'),
        ]);
        $this->assertEmailQueueCount(0);
    }

    /**
     * Unit test for PASSBOLT-3406
     *
     * foreignKey becomes null after a user is deleted and the sync is done once.
     * Expected: no exception should be thrown
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_foreignKeyIsNull()
    {
        $userEntry = $this->mockDirectoryEntryUser(['fname' => 'sofia', 'lname' => 'sofia', 'foreign_key' => 'null']);
        $this->mockDirectoryEntryGroup('freelancer');
        $this->mockDirectoryGroupData('freelancer', [
            'group_users' => [
                $userEntry->directory_name,
            ],
        ]);
        $this->mockDirectoryRelationGroupUser('freelancer', 'sofia');
        $reports = $this->action->execute();
        $this->assertTrue($reports->isEmpty());
        $this->assertEmailQueueCount(0);
    }

    /**
     * Unit test for PB-767
     *
     * When multiple users are added to a group, a failed validation on one groupUser should not make the other associations fail.
     *
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_failedValidationShouldNotContaminateOtherGroupUsers()
    {
        $this->mockDirectoryUserData('ruth', 'ruth', 'ruth@passbolt.com');
        $this->mockDirectoryUserData('betty', 'betty', 'betty@passbolt.com');
        // Ruth is a inactive user.
        $ruthEntry = $this->mockDirectoryEntryUser(['fname' => 'ruth', 'lname' => 'ruth', 'foreign_key' => UuidFactory::uuid('user.id.ruth')]);
        $bettyEntry = $this->mockDirectoryEntryUser(['fname' => 'betty', 'lname' => 'betty', 'foreign_key' => UuidFactory::uuid('user.id.betty')]);
        $this->mockDirectoryEntryGroup('marketing');
        //$this->mockDirectoryEntryGroup('marketing');
        $this->mockDirectoryGroupData('marketing', [
            'group_users' => [
                $ruthEntry->directory_name,
                $bettyEntry->directory_name,
            ],
        ]);
        $reports = $this->action->execute();

        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_IGNORE,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);

        $expectedUserGroupReport = [
            'action' => Alias::ACTION_CREATE,
            'model' => Alias::MODEL_GROUPS_USERS,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[1], $expectedUserGroupReport);
        $this->assertEmailQueueCount(0);
    }

    /**
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_HandlesCaseInsensitiveDn()
    {
        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $this->mockDirectoryUserData(
            'betty',
            'betty',
            'betty@passbolt.com',
            null,
            null,
            false
        );
        $francesEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $bettyEntry = $this->mockDirectoryEntryUser(['fname' => 'betty', 'lname' => 'betty', 'foreign_key' => UuidFactory::uuid('user.id.betty')]);
        $this->mockDirectoryEntryGroup('marketing');
        $this->mockDirectoryGroupData('marketing', [
            'group_users' => [
                $francesEntry->directory_name,
                strtolower($bettyEntry->directory_name),
            ],
        ]);

        $reports = $this->action->execute();

        $expectedUserGroupReport = [
            'model' => Alias::MODEL_GROUPS_USERS,
            'action' => Alias::ACTION_CREATE,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[0], $expectedUserGroupReport);

        $expectedUserGroupReport = [
            'model' => Alias::MODEL_GROUPS_USERS,
            'action' => Alias::ACTION_CREATE,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($reports[1], $expectedUserGroupReport);
    }

    /**
     * @group DirectorySync
     * @group DirectorySyncGroupUser
     * @group DirectorySyncGroupUserAdd
     */
    public function testDirectorySyncGroupUser_CaseSensitiveDnConfig()
    {
        Configure::write('passbolt.plugins.directorySync.caseSensitiveFilters', true);

        $this->mockDirectoryUserData('frances', 'frances', 'frances@passbolt.com');
        $this->mockDirectoryUserData('betty', 'betty', 'betty@passbolt.com');
        $francesEntry = $this->mockDirectoryEntryUser(['fname' => 'frances', 'lname' => 'frances', 'foreign_key' => UuidFactory::uuid('user.id.frances')]);
        $bettyEntry = $this->mockDirectoryEntryUser(['fname' => 'betty', 'lname' => 'betty', 'foreign_key' => UuidFactory::uuid('user.id.betty')]);
        $this->mockDirectoryEntryGroup('marketing');
        $this->mockDirectoryGroupData('marketing', [
            'group_users' => [
                $francesEntry->directory_name,
                strtolower($bettyEntry->directory_name),
            ],
        ]);

        $reports = $this->action->execute();

        $this->assertCount(1, $reports);
        /** @var \Passbolt\DirectorySync\Actions\Reports\ActionReport $result */
        $result = $reports[0];
        $expectedUserGroupReport = [
            'model' => Alias::MODEL_GROUPS_USERS,
            'action' => Alias::ACTION_CREATE,
            'status' => Alias::STATUS_SUCCESS,
            'type' => Alias::MODEL_GROUPS,
        ];
        $this->assertReport($result, $expectedUserGroupReport);
        $this->assertStringContainsString(
            'The user frances@passbolt.com was successfully added to the group Marketing',
            $result->getMessage()
        );
    }
}
